# BudgetBox

Ce projet a été généré avec [Angular CLI](https://github.com/angular/angular-cli) version 11.0.4

## Lancer le projet

Step 1: `npm run install`

Step 2: `npm run start`

## Lancer test unitaire

Exécutez `ng test` pour exécuter les tests de bout en bout via [Karma](https://karma-runner.github.io).

## Lancer test e2e

Exécutez `ng e2e` pour exécuter les tests de bout en bout via [Protractor](http://www.protractortest.org/).

## List des tests unitaire et tests d'intégrations

product.service.spec.ts
- API GET et PUT

product-item.component.spec.ts 
- Le composant enfant est bien initialisé

edit-product.component.spec.ts
- Tester la navigation avec params
- Tester si l'utilisateur clique sur le bouton 'Delete', une pop-up pour confirmation s'affiche
- Tester si le product est supprimé avec succès, la page navigue automatique à la page de la liste de produits
- Tester l'utilisateur peut annuler la confirmation

create-product.component.spec.ts
- Tester le formulaire de création est invalid, s'il est vide
- Tester le formulaire doit avoir 4 input tels que name, scientificName, groupId, subGroupId
- Tester le nouveau nom du produit doit être identique au nom du produit simplement supprimé (s'il y a) 