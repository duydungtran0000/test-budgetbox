import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';
import { ProductsComponent } from './products/products.component';
import { ProductListComponent } from './products/product-list/product-list.component';
import { ProductItemComponent } from './products/product-list/product-item/product-item.component';
import { CreateProductComponent } from './products/create-product/create-product.component';
import { MaterialModule } from './material.module';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { EditProductComponent } from './products/edit-product/edit-product.component';
import { DialogConfirmComponent } from './shared/components/dialog-confirm/dialog-confirm.component';
import { ProductService } from './products/product.service';
import { LocalStorageService } from './shared/services/local-storage/local-storage.service';
import { AlertService } from 'services/alert/alert.service';
import { AlertComponent } from 'components/alert/alert.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ProductsComponent,
    ProductListComponent,
    ProductItemComponent,
    EditProductComponent,
    CreateProductComponent,
    DialogConfirmComponent,
    AlertComponent
  ],
  imports: [
    HttpClientModule,
    MaterialModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
  ],
  providers: [ProductService, LocalStorageService, AlertService],
  bootstrap: [AppComponent],
})
export class AppModule {}
