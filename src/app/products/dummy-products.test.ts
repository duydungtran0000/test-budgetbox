import { Product } from './product.model';

export const dummyProductsForTesting = {
  hits: [
    {
      id: 917,
      name: 'Leek',
      scientificName: 'Allium porrum',
      groupId: 134,
      subGroupId: 138,
    },
    {
      id: 918,
      name: 'Garlic',
      scientificName: 'Allium sativum',
      groupId: 132,
      subGroupId: 133,
    },
    {
      id: 919,
      name: 'Chives',
      scientificName: 'Allium schoenoprasum',
      groupId: 132,
      subGroupId: 133,
    },
    {
      id: 921,
      name: 'Cashew nut',
      scientificName: 'Anacardium occidentale',
      groupId: 139,
      subGroupId: 139,
    },
    {
      id: 922,
      name: 'Pineapple',
      scientificName: 'Ananas comosus',
      groupId: 136,
      subGroupId: 137,
    },
  ],
  total: 904,
  subgroups: [
    {
      id: 133,
      name: 'Herbs',
    },
    {
      id: 137,
      name: 'Tropical fruits',
    },
    {
      id: 138,
      name: 'Onion-family vegetables',
    },
    {
      id: 139,
      name: 'Nuts',
    },
  ],
  groups: [
    {
      id: 132,
      name: 'Herbs and Spices',
    },
    {
      id: 134,
      name: 'Vegetables',
    },
    {
      id: 136,
      name: 'Fruits',
    },
    {
      id: 139,
      name: 'Nuts',
    },
  ],
};
