import { TestBed } from '@angular/core/testing';

import { ProductService } from './product.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { dummyProductsForTesting } from './dummy-products.test';
import { Product } from './product.model';

describe('ProductService', () => {
  let service: ProductService;
  let httpMock: HttpTestingController;
  let dummyProducts: any = dummyProductsForTesting;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProductService],
    });
    service = TestBed.inject(ProductService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should retrieve products from the API via GET', () => {
    service.fetchProducts(0, 5).subscribe((products) => {
      expect(products.hits.length).toBe(5);
      expect(products.hits).toEqual(dummyProducts.hits);
      expect(products.total).toBe(904);
      expect(products.subgroups.length).toBe(4);
      expect(products.groups.length).toBe(4);
    });

    const req = httpMock.expectOne(
      `${service.productURL}?from=${0}&size=${5}&includes=groups,subgroups`
    );

    expect(req.request.method).toBe('GET');

    req.flush(dummyProducts);
  });

  it('should modify product from the API via PUT', () => {
    const modifiedProduct: Product = {
      id: 918,
      name: 'Garlic',
      scientificName: 'Allium sativum',
      groupId: 132,
      subGroupId: 133,
    };

    service.updateProduct(modifiedProduct.id, modifiedProduct).subscribe((product) => {
      expect(product).toBe(modifiedProduct);
    });

    const req = httpMock.expectOne(
      `${service.productURL}${modifiedProduct.id}`
    );

    expect(req.request.method).toBe('PUT');
    expect(req.request.responseType).toEqual('json');

    req.flush(modifiedProduct);
  });
});
