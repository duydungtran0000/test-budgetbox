import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Product } from '../product.model';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {
  products: Product[];
  productsSub: Subscription;
  totalSub: Subscription;
  total: number;
  from: number = 0;
  pageIndex: number= 0
  itemPerPage: number = 20;
  breakpoint: number;

  constructor(private productService: ProductService, private router: Router, private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.breakpoint = window.innerWidth <= 800 ? 2 : 5;
    this.activatedRoute.queryParams.subscribe(params => {
      const pageIndex = params['page'];
      if(pageIndex) {
        this.pageIndex = pageIndex
        this.from = this.getStartIndex(parseInt(pageIndex), this.itemPerPage)
      }
    });
    localStorage.setItem('pageIndex', this.from.toString()) 
    this.productService.fetchProducts(this.from, this.itemPerPage).subscribe();
    this.productsSub = this.productService.productsChanged.subscribe(
      (products: Product[]) => (this.products = products)
    );
    this.totalSub = this.productService.totalChanged.subscribe(
      (total: number) => (this.total = total)
    );
  }

  onResize(event) {
    this.breakpoint = event.target.innerWidth <= 800 ? 2 : 5;
  }

  ngOnDestroy(): void {
    this.productsSub.unsubscribe();
    this.totalSub.unsubscribe();
  }

  getStartIndex(pageIndex: number, pageSize: number) {
    return pageIndex * pageSize
  }

  onPageChange(event: PageEvent) {
    this.router.navigate([''], { queryParams: { page: event.pageIndex } });
    localStorage.setItem('pageIndex', this.from.toString())
    const startIndex = this.getStartIndex(event.pageIndex, event.pageSize)

    this.productService.fetchProducts(startIndex, this.itemPerPage).subscribe();
  }
}
