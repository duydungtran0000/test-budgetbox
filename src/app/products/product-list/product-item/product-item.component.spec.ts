import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from 'app/material.module';
import { Product } from 'app/products/product.model';

import { ProductItemComponent } from './product-item.component';

describe('ProductItemComponent', () => {
  let component: ProductItemComponent;
  let fixture: ComponentFixture<ProductItemComponent>;
  const product: Product =  {
    id: 1,
    name: 'test', 
    scientificName: 'test',
    groupId: 123,
    subGroupId: 456
  }
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductItemComponent],
      imports: [MaterialModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductItemComponent);
    component = fixture.componentInstance;
    component.product = product
    fixture.detectChanges();
  });

  it('childrend should create', () => {
    expect(component).toBeTruthy();
  });
  
});
