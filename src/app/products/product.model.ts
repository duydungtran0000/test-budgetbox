export class Product {
    id: number;
    name: string;
    scientificName: string;
    groupId: number;
    subGroupId: number

    constructor(id: number, name: string, scientificName: string, groupId: number, subGroupId: number) {
        this.id = id
        this.name = name
        this.scientificName = scientificName
        this.groupId = groupId
        this.subGroupId = subGroupId
    }
}