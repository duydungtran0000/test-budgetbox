import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from './product.model';
import { Group } from './group.model';

import { Observable, Subject, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  productsChanged = new Subject<Product[]>();
  totalChanged = new Subject<number>();
  products: Product[]=[];
  total: number = 0;
  groups: Group[] = [];
  subGroups: Group[] = [];
  productsDeleted: any[] = []

  productURL = "http://localhost:8080/products/v1.0/"
  constructor(private http: HttpClient) {}

  fetchProducts(from: number, itemsPerPage: number) {
    return this.http
      .get(
        `${this.productURL}?from=${from}&size=${itemsPerPage}&includes=groups,subgroups`
      )
      .pipe(
        tap((data: any) => {
          this.setProducts(data.hits);
          this.setTotal(data.total);
          this.setGroups(data.groups);
          this.setSubGroups(data.subgroups);
        }),
        // catchError(this.handleError)
      );
  }

  createProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(this.productURL, product).pipe(
      catchError(this.handleError)
    )
  }

  deleteProduct(idProduct: number): Observable<any> {
    return this.http.delete<any>( `${this.productURL}${idProduct}`).pipe(
      catchError(this.handleError)
    )
  }

  updateProduct(id: number, product:Product): Observable<Product> {
    return this.http.put<Product>( `${this.productURL}${id}`, product).pipe(
      catchError(this.handleError)
    )
  }

  handleError(error) {
    return throwError(error.message || "Server error")
  }

  setProducts(products: Product[]) {
    this.products = products;
    this.productsChanged.next(this.products.slice());
  }

  // addProduct(product: Product) {
  //   this.products.push(product)
  //   this.productsChanged.next(this.products.slice());
  // }

  getProducts() {
    return this.products.slice();
  }

  getTotal() {
    return this.total;
  }

  setTotal(total: number) {
    this.total = total;
    this.totalChanged.next(this.total);
  }

  getGroups() {
    return this.groups;
  }

  setGroups(groups: Group[]) {
    this.groups = groups;
  }

  getSubGroups() {
    return this.subGroups;
  }

  setSubGroups(groups: Group[]) {
    this.subGroups = groups;
  }

  getDetailProductById(id: number) {
    if (!this.products) throw Error('Product is null/undefined');
    const product = this.products.find((item) => item.id == id);

    if (!product) throw Error("Product doesn't exist");
    const group = this.getGroupById(product.groupId);
    const subGroup = this.getSubGroupById(product.subGroupId);

    if (!group || !subGroup) throw Error("Group doesn't exist");

    return {
      id: product.id,
      name: product.name,
      scientificName: product.scientificName,
      group: {
        ...group,
      },
      subGroup: {
        ...subGroup,
      },
    };
  }

  getGroupById(idGroup: number) {
    if (!this.groups) return null;
    return this.groups.find((item) => item.id == idGroup);
  }

  getSubGroupById(idSubGroup: number) {
    if (!this.subGroups) return null;

    return this.subGroups.find((item) => item.id == idSubGroup);
  }

  getProductsDeleted() {

  }

  setProductsDeteled(product: any) {
    this.productsDeleted.push(product)
    // localStorage.setItem('productsDeleted', this.productsDeleted)
  }
}

