import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertService } from 'services/alert/alert.service';
import { LocalStorageService } from 'services/local-storage/local-storage.service';
import { Group } from '../group.model';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss'],
})
export class CreateProductComponent implements OnInit {
  public createForm: FormGroup;
  public groups: Group[] = [];
  public subGroups: Group[] = [];
  public nameProductDeleted: string = null;

  constructor(
    private productService: ProductService,
    private localStorageService: LocalStorageService,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.groups = this.productService.getGroups();
    this.subGroups = this.productService.getSubGroups();

    this.createForm = new FormGroup({
      name: new FormControl(null, [
        Validators.required,
        this.isEqualWithNameDeleted.bind(this),
      ]),
      scientificName: new FormControl(null, Validators.required),
      groupId: new FormControl(null, Validators.required),
      subGroupId: new FormControl(null, Validators.required),
    });

    this.localStorageService.get('productsDeleted').then((data: any) => {
      if (data) {
        this.nameProductDeleted = data;
        // this.name = this.nameProductDeleted;
        this.createForm.controls.name.setValue(this.nameProductDeleted);
      }
    });
  }

  onSubmit() {
    console.log(this.createForm.value);
    const payload = {
      ...this.createForm.value,
    };
    this.productService.createProduct(payload).subscribe(
      (response) => {
        if (!response) return;
        // console.log(response);
        this.alertService.success('Product created');
      },
      (error) => {
        console.error(error);
        this.alertService.error(error);
      }
    );
  }

  isEqualWithNameDeleted(
    control: FormControl
  ): { [key: string]: boolean } | null {
    if (this.nameProductDeleted) {
      if (this.nameProductDeleted !== control.value) {
        return { nameValidator: true };
      }
    }
    return null;
  }
}
