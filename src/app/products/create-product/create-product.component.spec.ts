import 'zone.js/dist/zone-testing';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateProductComponent } from './create-product.component';
import { MaterialModule } from 'app/material.module';
import { AlertComponent } from 'components/alert/alert.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProductService } from '../product.service';
import { LocalStorageService } from 'services/local-storage/local-storage.service';
import { AlertService } from 'services/alert/alert.service';

var db: any;

describe('CreateProductComponent', () => {
  let component: CreateProductComponent;
  let fixture: ComponentFixture<CreateProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateProductComponent, AlertComponent ],
      imports: [
        HttpClientModule,
        RouterModule.forRoot([]),
        MaterialModule,
        ReactiveFormsModule,
        BrowserAnimationsModule
      ],
      providers: [ProductService, LocalStorageService, AlertService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    
  });

  it('component should be create', () => {
    expect(component).toBeTruthy();
  });
  
  it('form invalid when empty', () => {
    expect(component.createForm.valid).toBeFalse()
  });

  it('should create form with 4 controls', () => {
    expect(component.createForm.contains('name')).toBeTruthy()
    expect(component.createForm.contains('scientificName')).toBeTruthy()
    expect(component.createForm.contains('groupId')).toBeTruthy()
    expect(component.createForm.contains('subGroupId')).toBeTruthy()
  });

  it('new product name should be the last product name to be deleted', () => {
    component.nameProductDeleted = 'World'

    let name = component.createForm.get('name')
    name.setValue('Hello')

    expect(name.valid).toBeFalse()
  });

});
