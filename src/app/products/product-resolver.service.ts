import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { Product } from './product.model';
import { ProductService } from './product.service';

@Injectable({ providedIn: 'root' })
export class ProductResolverService implements Resolve<Product[]> {
  constructor(private productService: ProductService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const products = this.productService.getProducts();

    if (products.length === 0) {
      const pageIndex = localStorage.getItem('pageIndex');
      console.log(pageIndex);
      return this.productService.fetchProducts(parseInt(pageIndex), 20);
    } else {
      return products;
    }
  }
}
