import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { DialogConfirmComponent } from 'app/shared/components/dialog-confirm/dialog-confirm.component';
import { AlertService } from 'services/alert/alert.service';
import { Group } from '../group.model';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss'],
})
export class EditProductComponent implements OnInit {
  public id: number;
  @ViewChild('f') createForm: NgForm;
  public detailProduct: any;
  public groups: Group[] = [];
  public subGroups: Group[] = [];
  public selectedGroupId: number;
  public selectedSubGroupId: number;
  public dialogRef: MatDialogRef<DialogConfirmComponent>;

  constructor(
    private alert: AlertService,
    private productService: ProductService,
    private router: Router,
    public activatedRoute: ActivatedRoute,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.id = params['id'];
    });
    try {
      this.detailProduct = this.productService.getDetailProductById(this.id);
      this.selectedGroupId = this.detailProduct.group.id;
      this.selectedSubGroupId = this.detailProduct.subGroup.id;
    } catch (e) {
      // console.error(e)
      this.alert.error(e);
    }
    this.groups = this.productService.getGroups();
    this.subGroups = this.productService.getSubGroups();
  }

  onSubmit() {
    const payload = {
      ...this.createForm.value,
      id: this.id,
    };
    // this.alert.success('Product saved')

    this.productService.updateProduct(this.id, payload).subscribe(
      (response) => {
        if (!response) return;
        this.alert.success('Product saved');
      },
      (error) => {
        this.alert.error(error);
      }
    );
  }

  delete() {
    this.dialogRef = this.dialog.open(DialogConfirmComponent, {
      width: '350px',
      data: this.detailProduct,
    });

    this.dialogRef.afterClosed().subscribe((isDeleted) => {
      if (isDeleted) this.router.navigate(['./']);
    });
  }
}
