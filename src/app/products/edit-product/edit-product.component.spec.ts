import 'zone.js/dist/zone-testing';
import { MaterialModule } from 'app/material.module';

import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { MatDialogRef } from '@angular/material/dialog';

import { ProductService } from '../product.service';
import { LocalStorageService } from 'services/local-storage/local-storage.service';
import { AlertService } from 'services/alert/alert.service';

import { EditProductComponent } from './edit-product.component';
import { AlertComponent } from 'components/alert/alert.component';
import { DialogConfirmComponent } from 'components/dialog-confirm/dialog-confirm.component';

import { Observable, of, Subject } from 'rxjs';

var db: any;

// for test route param
class ActivatedRouteStub {
  private subject = new Subject();

  // params: Observable<any> = of([])
  push(value:any) {
    this.subject.next(value)
  }

  get params() {
    return this.subject.asObservable()
  }
}

describe('EditProductComponent', () => {
  let component: EditProductComponent;
  let fixture: ComponentFixture<EditProductComponent>;
  let router: Router;

  const testCases = [
    {
      message: 'if successful, navigate to the homepage',
      isDeleted: true,
    },
    {
      message: 'cancel',
      isDeleted: false,
    },
  ];
  // let params: Subject<Params>
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        EditProductComponent,
        AlertComponent,
        DialogConfirmComponent,
      ],
      imports: [
        HttpClientModule,
        RouterTestingModule.withRoutes([
          { path: ':id/edit', component: EditProductComponent }
      ]),
        MaterialModule,
        FormsModule,
        BrowserAnimationsModule,
      ],
      providers: [
        ProductService,
        LocalStorageService,
        AlertService,
        {provide: ActivatedRoute, useClass: ActivatedRouteStub}
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.inject(Router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be open confirm dialog when click on button delete', async () => {
    spyOn(component.dialog, 'open').and.callThrough();
    let button = fixture.debugElement.nativeElement.querySelector(
      'button[type="button"]'
    );
    button.click();

    expect(component.dialog.open).toHaveBeenCalledWith(DialogConfirmComponent, {
      width: '350px',
      data: component.detailProduct,
    });
  });

  testCases.forEach((testCase) => {
    it(`should open the export matDialog and ${testCase.message}`, () => {
      let spyNavigate = spyOn(router, 'navigate');

      spyOn(component.dialog, 'open').and.returnValue({
        afterClosed: () => of(testCase.isDeleted),
      } as MatDialogRef<typeof component>);

      component.delete();

      if (testCase.isDeleted) {
        expect(spyNavigate).toHaveBeenCalledWith(['./']);
      } else {
        expect(spyNavigate).not.toHaveBeenCalled();
      }
    });
  });

  it('should get route param', () => {
    let route: ActivatedRouteStub = TestBed.inject(ActivatedRoute) as any
    const id = 911
    route.push({id: id})
    
    expect(component.id).toEqual(id)
  });
});
