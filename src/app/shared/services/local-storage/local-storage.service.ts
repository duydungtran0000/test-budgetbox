import { Injectable } from '@angular/core';

declare var db: any;

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  public storageName = 'BudgetBox';
  constructor() {}

  // add and update
  add(keyname: any, value: any) {

    return new Promise( async (resolve, reject) => {
      if (db === undefined) reject('db is undefined');

      const request = db
        .transaction([this.storageName], 'readwrite')
        .objectStore(this.storageName)
        .put(value, keyname);

      request.onsuccess = await function (event) {
        if (event.target.result) {
          console.log('success')
          resolve('success')
        } else {
          console.log('error')
          resolve(false);
        }
      };
    });
  }

  // get the data from indexDB
  get(keyname: any) {
    return new Promise( async (resolve, reject) => {
      if (db === undefined) reject('db is undefined');

      const request = db
        .transaction([this.storageName], 'readwrite')
        .objectStore(this.storageName)
        .get(keyname);

      request.onsuccess = await function (event) {
        if (event.target.result) {
          console.log('success')
          resolve(event.target.result)
        } else {
          console.log('error')
          resolve(false);
        }
      };
    });
  }

  // delete the data
  delete() {}
}
