import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProductService } from 'app/products/product.service';
import { AlertService } from 'services/alert/alert.service';
import { LocalStorageService } from 'services/local-storage/local-storage.service';

@Component({
  selector: 'app-dialog-confirm',
  templateUrl: './dialog-confirm.component.html',
  styleUrls: ['./dialog-confirm.component.scss'],
})
export class DialogConfirmComponent implements OnInit {
  constructor(
    private productService: ProductService,
    private localStorageService: LocalStorageService,
    public dialogRef: MatDialogRef<DialogConfirmComponent>,
    private alertService: AlertService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onDelete(): void {
    this.productService.deleteProduct(this.data.id).subscribe(
      (res) => {
        this.localStorageService
          .add('productsDeleted', this.data.name)
          .then((res) => {
            this.alertService.success('Product deleted');
            setTimeout(() => this.dialogRef.close(true), 1000);
          });
      },
      (error) => this.alertService.error(error)
    );
    // this.alertService.success('Product deleted');
  }
}
