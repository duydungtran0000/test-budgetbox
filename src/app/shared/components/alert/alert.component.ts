import { Component, OnInit } from '@angular/core';
import { AlertService } from 'services/alert/alert.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
})
export class AlertComponent implements OnInit {
  private duration: number = 3000;
  private verticalPosition: any = 'bottom';

  constructor(
    private alertService: AlertService,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.alertService.getMessage().subscribe((message) => {
      console.log(message)
      if (message != null) {
        // there is a message to show, so change snackbar style to match the message type
        if (message.type === 'error') {
          this.snackBar.open(message.text, undefined, {
            duration: this.duration,
            verticalPosition: this.verticalPosition,
            panelClass: ['alert-error'],
          });
        } else if (message.type === 'success') {
          this.snackBar.open(message.text, undefined, {
            duration: this.duration,
            verticalPosition: this.verticalPosition,
            panelClass: ['alert-success'],
          });
        } else {
          this.snackBar.open(message.text, undefined, {
            duration: this.duration,
            verticalPosition: this.verticalPosition,
          });
        }
      }
    });
  }
}
