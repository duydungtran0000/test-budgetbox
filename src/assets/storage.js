window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB

window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction

window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange

if (!window.indexedDB) {
    alert('Your browser is not supported IndexDB')
}

var db

var request = window.indexedDB.open('budgetbox')

request.onerror = function (event) {
    console.error(event.target.result)
}

request.onsuccess = function (event) {
    db = request.result
    console.log('success' + db)
}

request.onupgradeneeded = function (event) {
    var db = event.target.result
    var objectStore = db.createObjectStore('BudgetBox')
}